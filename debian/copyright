Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: SeqAn
Upstream-Contact: Manuel Holtgrewe <manuel.holtgrewe@fu-berlin.de>
                  Hannes Röst <roest@imsb.biol.ethz.ch>
Source: http://packages.seqan.de/
Files-Excluded: .*
                submodules/sdsl-lite/external/d3/d3.min.js
                submodules/cereal
#               submodules/range-v3
# when upstream makes a new release and that finds its way to Debian

Files: *
Copyright: 2006-2016, Knut Reinert, FU Berlin
           Andreas Döring <doering@inf.fu-berlin.de>
           David Weese <weese@inf.fu-berlin.de>
           Tobias Rausch <rausch@inf.fu-berlin.de>
           Stephan Aiche <aiche@inf.fu-berlin.de>
           Anne-Katrin Emde <emde@inf.fu-berlin.de>
           Carsten Kemena <kemena@inf.fu-berlin.de>
           Konrad Ludwig Moritz Rudolph <krudolph@inf.fu-berlin.de>
           Marcel H. Schulz. <marcel.schulz@molgen.mpg.de>
           Manuel Holtgrewe <manuel.holtgrewe@fu-berlin.de>
           Hannes Röst <roest@imsb.biol.ethz.ch>
           2018, the SDSL Project Authors.
License: BSD-3-clause

Files: include/seqan3/contrib/stream/bgzf_istream.hpp
       include/seqan3/contrib/stream/gz_ostream.hpp
       include/seqan3/contrib/stream/bgzf_ostream.hpp
       include/seqan3/contrib/stream/bz2_istream.hpp
       include/seqan3/contrib/stream/bz2_ostream.hpp
       include/seqan3/contrib/stream/gz_istream.hpp
Copyright: 2003 Jonathan de Halleux
           2014 David Weese <dave.weese@gmail.com>
License: zlib
 This software is provided 'as-is', without any express or implied warranty. In
 no event will the authors be held liable for any damages arising from the use
 of this software.
 .
 Permission is granted to anyone to use this software for any purpose, including
 commercial applications, and to alter it and redistribute it freely, subject to
 the following restrictions:
 .
  1. The origin of this software must not be misrepresented; you must not claim
     that you wrote the original software. If you use this software in a
     product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution

Files: submodules/sdsl-lite/*
Copyright: 2007-2014 Simon Gog
           2016, 2018 the SDSL Project Authors
           2015, THL A29 Limited, a Tencent company, and Milo Yip
           2014, Juha Karkkainen, Dominik Kempa and Simon J. Puglisi
           2003-2008, Yuta Mori
License: GPL-3+

Files: debian/*
Copyright: 2016 Michael R. Crusoe <crusoe@ucdavis.edu>
                Kevin Murray <spam@kdmurray.id.au>
                Andreas Tille <tille@debian.org>
License: BSD-3-clause

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Knut Reinert or the FU Berlin nor the names of
      its contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL KNUT REINERT OR THE FU BERLIN BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 DAMAGE.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the Free
 Software Foundation, either version 3 of the License, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program.  If not, see <http://www.gnu.org/licenses/>.
Comment:
 On Debian systems, you can find the GPL license version 3 in
 ‘/usr/share/common-licenses/GPL-3’.

Files: submodules/range-v3/*
License: BSL and NSCA and X11 and Unnamed1
Copyright: 2009 Alexander Stepanov
           2014 Andrew Sutton
           2019 Andrey Diduh
           2019 Barry Revzin
           2015-2019 Casey Carter
           2019 Christopher Di Bella
           1999 Dave Abrahams
           2004,2013-2019 Eric Niebler
           2015,2017 Filip Matzner
           2014-2017 Gonzalo Brito Gadeschi
           2016 Jakub Szuppe
           2019 Johel Guerrero
           2015 Julian Becker
           2006,2008 Junio C Hamano
           1999 Kevlin Henney
           2013-2017 Louis Dionne
           2016 Luis Martinez de Bartolome Izquierdo
           2015 Manu Sánchez
           2015 Melissa E. O'Neill
           2014 Michel Morin
           2018-2019 MikeGitb
           2015-2016 Paul Fultz II
           2009 Paul McJones
           2017 Rostislav Khlebnikov
           2016 Tobias Mayer
           2015-2016 Tomislav Ivek
           2005-2007 Adobe Systems Incorporated
           2018-2019 Facebook, Inc.
           1994 Hewlett-Packard Company
           2009-2014 LLVM Team
           2015 Microsoft Corporation
           1996 Silicon Graphics Computer Systems, Inc.

Files: submodules/range-v3/.gitignore
License: X11
Copyright: 2013 GitHub, Inc.


Files: submodules/range-v3/example/*
License: BSL
Copyright: 2019 Eric Niebler <eniebler@boost.org>

License: BSL
 Permission is hereby granted, free of charge, to any person or organization
 obtaining a copy of the software and accompanying documentation covered by this
 license (the "Software") to use, reproduce, display, distribute, execute, and
 transmit the Software, and to prepare derivative works of the Software, and to
 permit third-parties to whom the Software is furnished to do so, all subject to
 the following:
 .
 The copyright notices in the Software and this entire statement, including the
 above license grant, this restriction and the following disclaimer, must be
 included in all copies of the Software, in whole or in part, and all derivative
 works of the Software, unless such copies or derivative works are solely in the
 form of machine-executable object code generated by a source language
 processor.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE FOR ANY
 DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: NSCA
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal with
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is furnished to do
 so, subject to the following conditions:
 .
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimers.
 .
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimers in the documentation
      and/or other materials provided with the distribution.
 .
    * Neither the names of the LLVM Team, University of Illinois at
      Urbana-Champaign, nor the names of its contributors may be used to endorse
      or promote products derived from this Software without specific prior
      written permission.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE
 SOFTWARE.

License: X11
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is furnished to do
 so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: Unnamed1
 Permission to use, copy, modify, distribute and sell this software and its
 documentation for any purpose is hereby granted without fee, provided that the
 above copyright notice appear in all copies and that both that copyright notice
 and this permission notice appear in supporting documentation. The authors make
 no representations about the suitability of this software for any purpose. It
 is provided "as is" without express or implied warranty.
 .
 Permission to use, copy, modify, distribute and sell this software and its
 documentation for any purpose is hereby granted without fee, provided that the
 above copyright notice appear in all copies and that both that copyright notice
 and this permission notice appear in supporting documentation. Hewlett-Packard
 Company makes no representations about the suitability of this software for any
 purpose. It is provided "as is" without express or implied warranty.
 .
 Permission to use, copy, modify, distribute and sell this software and its
 documentation for any purpose is hereby granted without fee, provided that the
 above copyright notice appear in all copies and that both that copyright notice
 and this permission notice appear in supporting documentation. Silicon
 Graphics makes no representations about the suitability of this software for
 any purpose. It is provided "as is" without express or implied warranty.
