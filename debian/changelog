seqan3 (3.0.2~rc1+bf04354+ds-1) unstable; urgency=medium

  [ Michael R. Crusoe ]
  * Update patch headers
  * Package pre-release from upstream to fix gcc 10.2 FTBFS.
    Closes: #966270, #966433
  * debhelper-compat 13 (routine-update)
  * Switch to upstream's fork of range-v3

 -- Dimitri John Ledkov <xnox@ubuntu.com>  Tue, 08 Sep 2020 14:59:35 +0100

seqan3 (3.0.1+ds-10) unstable; urgency=medium

  * Include patch from upstream for Doxygen 1.8.17 compatability. Closes: #959645

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Mon, 04 May 2020 13:43:35 +0200

seqan3 (3.0.1+ds-9) unstable; urgency=medium

  * Fix FTBFS with patch from upstream. Closes: #954589
  * Standards-Version: 4.5.0 (routine-update)
  * Add salsa-ci file (routine-update)
  * Wrap long lines in changelog entries: 3.0.0+ds2-4.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Wed, 25 Mar 2020 22:01:41 +0100

seqan3 (3.0.1+ds-8) unstable; urgency=medium

  * Fix the arm64 patch.

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Wed, 05 Feb 2020 17:06:55 +0100

seqan3 (3.0.1+ds-7) unstable; urgency=medium

  * Patch around erroneous test that affected arm64.

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Wed, 05 Feb 2020 16:38:35 +0100

seqan3 (3.0.1+ds-6) unstable; urgency=medium

  * Properly pass the flag to cmake

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sat, 01 Feb 2020 14:16:00 +0100

seqan3 (3.0.1+ds-5) unstable; urgency=medium

  * Oops, meant the previous release to be arch: all, not arch:any!
    This maximizes the potential architecture support for apps that use seqan3

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sat, 01 Feb 2020 10:10:00 +0100

seqan3 (3.0.1+ds-4) unstable; urgency=medium

  * change libseqan3-dev to architecture: any
    This maximizes the potential architecture support for apps that use seqan3
  * Drop build-dep on ccache

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sat, 01 Feb 2020 09:01:47 +0100

seqan3 (3.0.1+ds-3) unstable; urgency=medium

  * Autopkgtests: correct {CXX,CPP,C}FLAGS exports

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sun, 26 Jan 2020 23:36:12 +0100

seqan3 (3.0.1+ds-2) unstable; urgency=medium

  * Autopkgtests: set the same flags as build time

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Fri, 24 Jan 2020 14:23:27 +0100

seqan3 (3.0.1+ds-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.4.1

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Thu, 23 Jan 2020 18:38:07 +0100

seqan3 (3.0.0+ds2-5) unstable; urgency=medium

  * Also drop liblemon-dev from the -dev dependency
  * new patch: int8_t -> char for the magic byte sequences
  * Support the nocheck and/or the nodoc build profiles
  * Fix header autopkgtest

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Fri, 29 Nov 2019 07:30:57 +0100

seqan3 (3.0.0+ds2-4) unstable; urgency=medium

  * debian/patches/arch-indep-spins: add support for armel armhf arm64
  * debian/tests/control: explicitly depend on g++
  * debian/rules: arm* -Wno-narrowing -Wno-maybe-uninitialized
  * debian/{control,rules} Try out test/header and test/performance with
    libbenchmark-dev 1.5+
  * Set upstream metadata fields: Repository.
  * Remove obsolete fields Name, Contact from debian/upstream/metadata.
  * AutoPkgTests: include the header tests

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Wed, 27 Nov 2019 12:08:49 +0100

seqan3 (3.0.0+ds2-3) unstable; urgency=medium

  * Remove unused buil-dep on liblemon-dev
  * Only install the docs in arch-independent builds, put them in
    /usr/share/doc/libseqan3-dev/

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Mon, 25 Nov 2019 14:35:44 +0100

seqan3 (3.0.0+ds2-2) unstable; urgency=medium

  * Build on each arch, to confirm compatability.
  * -doc: mark as Multi-arch: foreign

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sun, 17 Nov 2019 17:55:32 +0100

seqan3 (3.0.0+ds2-1) unstable; urgency=medium

  * Initial upload. (Closes: #934629)

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Mon, 12 Aug 2019 14:05:10 +0200
